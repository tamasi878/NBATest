//
//  NBATestTests.swift
//  NBATestTests
//
//  Created by Tamási Móni on 21/08/16.
//  Copyright © 2016 Tamási. All rights reserved.
//

import XCTest

@testable import NBATest

class NBATestTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testTeamRemote() {
        DataManager.sharedManager.getTeamsFromServer()
            {
            (success: Bool, error: NSError?) in
            if success {
                XCTAssert(true)
            }
            else {
                print(error?.localizedDescription)
                XCTAssert(false)
            }
        }

    }
    
    func testPlayerRemote() {
        DataManager.sharedManager.getPlayersOfTeamFromServer("nba-det", isFirstPage: true)
            {
                (success: Bool, error: NSError?) in
                if success {
                    XCTAssert(true)
                }
                else {
                    print(error?.localizedDescription)
                    XCTAssert(false)
                }
        }
        
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
    
}
