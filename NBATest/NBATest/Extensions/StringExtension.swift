//
//  StringExtension.swift
//  museum
//
//  Created by Tamási Mónika on 2016. 04. 18..
//  Copyright © 2016. Zsolt Majoros. All rights reserved.
//

import Foundation
import UIKit

extension String
{
    /**
     * Returns the estimated height of a label
     * - parameter width: width of the label
     * - parameter font: font of the label
     * - return value: the estimated height
     */
    func heightWithConstrainedWidth(width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: CGFloat.max)
        let boundingBox = self.boundingRectWithSize(constraintRect, options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
        
        return boundingBox.height
    }
    
    /**
     * returns the length of a string
     * - return value: length of a string
     */
    func length() -> Int
    {
        return self.characters.count
    }
    
    /**
     * Search for the string at the beginning of this string
     * - return value: true if the it starts with the string, false if not
     */
    func startsWith(string: String) -> Bool {
        
        guard let range = rangeOfString(string, options:[.AnchoredSearch, .CaseInsensitiveSearch]) else {
            return false
        }
        
        return range.startIndex == startIndex
    }
}