//
//  NSURLExtension.swift
//  museum
//
//  Created by Tamási Mónika on 2016. 05. 18..
//  Copyright © 2016. Zsolt Majoros. All rights reserved.
//

import Foundation
import UIKit

extension NSURL
{
    func URLByAppendingQueryString(queryString: String) -> NSURL
    {
        if queryString.length() == 0 {
            return self
        }
    
        let URLString = self.absoluteString + "?" + queryString
        let url = NSURL(string: URLString)
        return url!
    }
}