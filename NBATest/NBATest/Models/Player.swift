//
//  Player.swift
//  NBATest
//
//  Created by Tamási Móni on 21/08/16.
//  Copyright © 2016 Tamási. All rights reserved.
//

import Foundation
import SwiftyJSON
import UIKit

public class Player
{
    var id: String
    var name: String
    var firstName: String
    var draftTeam: String
    var uniformNumber: String
    var height: Int
    var heightUnit: String
    var position: String
    var salary: Int
    var salaryUnit: String
    var weight: Int
    var weightUnit: String
    var experience: Int
    
    init(json: JSON) {
        self.id = json["id"].stringValue
        self.name = json["name"].stringValue
        self.firstName = json["first_name"].stringValue
        self.draftTeam = json["draft_team_name"].stringValue
        self.uniformNumber = json["uniform_number"].stringValue
        self.height = json["height"].intValue
        self.heightUnit = json["unit_of_height"].stringValue
        self.position = json["position_name"].stringValue
        self.salary = json["salary"].intValue
        self.salaryUnit = json["salary_currency"].stringValue
        self.weight = json["weight"].intValue
        self.weightUnit = json["unit_of_weight"].stringValue
        self.experience = json["years_of_experience"].intValue
    }
    
}
