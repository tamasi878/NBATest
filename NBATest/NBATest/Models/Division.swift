//
//  Division.swift
//  NBATest
//
//  Created by Tamási Móni on 21/08/16.
//  Copyright © 2016 Tamási. All rights reserved.
//

import Foundation
import SwiftyJSON

public class Division {
    var id: String
    var name: String
    
    init(json: JSON) {
        self.id = json["id"].stringValue
        self.name = json["name"].stringValue
    }
    
    init(id: String, name: String) {
        self.id = id
        self.name = name
    }
}