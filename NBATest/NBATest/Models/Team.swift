//
//  Team.swift
//  NBATest
//
//  Created by Tamási Móni on 21/08/16.
//  Copyright © 2016 Tamási. All rights reserved.
//

import Foundation
import SwiftyJSON
import UIKit

public class Team {
    var id: String
    var location: String
    var name: String
    var nickname: String
    var divisionId: String
    var slug: String
    var color1: UIColor = UIColor.whiteColor()
    var color2: UIColor = UIColor.blackColor()
    
    init(json: JSON) {
        self.id = json["id"].stringValue
        self.location = json["location"].stringValue
        self.name = json["name"].stringValue
        self.nickname = json["nickname"].stringValue
        self.divisionId = json["division_id"].stringValue
        self.slug = json["slug"].stringValue
        if json["colors"].arrayValue.count > 0 {
            self.color1 = UIColor.colorWithHexString(json["colors"][0].stringValue)
        }
        if json["colors"].arrayValue.count > 1 {
            self.color2 = UIColor.colorWithHexString(json["colors"][1].stringValue)
        }
    }
    
    init(id: String, location: String, name: String, nickname: String, divisionId: String, slug: String) {
        self.id = id
        self.name = name
        self.location = location
        self.nickname = nickname
        self.divisionId = divisionId
        self.slug = slug
    }
}
