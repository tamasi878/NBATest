//
//  DataManager.swift
//  NBATest
//
//  Created by Tamási Móni on 21/08/16.
//  Copyright © 2016 Tamási. All rights reserved.
//

import Foundation
import SwiftyJSON

public class DataManager
{
    public static let sharedManager = DataManager()
    
    private var divisions = [Division]()
    private var teams = [Team]()
    private var players = [Player]()
    private var currentPage: Int = 1
    private var isLastPage: Bool = false
    private var currentPlayerPage: Int = 1
    private var isLastPlayerPage: Bool = false
    
    public func getTeams() -> [Team]!
    {
        return self.teams
    }
    
    public func getPlayers() -> [Player]!
    {
        return self.players
    }
    
    public func getDivisionName(divisionId: String) -> String
    {
        let divs = self.divisions.filter{ $0.id == divisionId }
        if divs.count > 0 {
            return divs[0].name
        }
        return ""
    }
    
    
    public func getTeamsFromServer(completion: (success: Bool, error: NSError?) -> Void)
    {
        if !self.isLastPage {
            NetworkManager.fetchTeams(self.currentPage) {
                (success: Bool, error: NSError?, response: JSON?) in
                    if success {
                        DataManager.sharedManager.evaluateTeamResult(response!)
                    }
                completion(success: success, error: error)
            }
        }
    }
    
    public func getPlayersOfTeamFromServer(team: String, isFirstPage: Bool, completion: (success: Bool, error: NSError?) -> Void)
    {
        if isFirstPage {
            self.currentPlayerPage = 1
            self.isLastPlayerPage = false
            self.players.removeAll()
        }
        if !self.isLastPlayerPage {
            NetworkManager.fetchPlayers(self.currentPlayerPage, team: team) {
                (success: Bool, error: NSError?, response: JSON?) in
                if success {
                    DataManager.sharedManager.evaluatePlayerResult(response!)
                }
                completion(success: success, error: error)
            }
        }
    }
}

extension DataManager
{
    private func evaluateTeamResult(response: JSON)
    {
        let divisions = response["divisions"]
        let teams = response["teams"]
        
        if teams.count == 0 || teams.count < TEAMS_PER_PAGE
        {
            self.isLastPage = true
        }
        
        for div: JSON in divisions.arrayValue {
            let division = Division(json: div)
            self.divisions.append(division)
        }
        
        for tm: JSON in teams.arrayValue {
            let team = Team(json: tm)
            self.teams.append(team)
        }
        
        self.currentPage += self.isLastPage ? 0 : 1
    }
    
    private func evaluatePlayerResult(response: JSON)
    {
        let plys = response["players"]
        
        if plys.count == 0 || plys.count < PLAYERS_PER_PAGE
        {
            self.isLastPlayerPage = true
        }
        
        for ply: JSON in plys.arrayValue {
            if ply["active"].boolValue {
                let player = Player(json: ply)
                self.players.append(player)
            }
        }
        
        self.currentPlayerPage += self.isLastPlayerPage ? 0 : 1
    }
}