//
//  NetworkManager.swift
//  NBATest
//
//  Created by Tamási Móni on 21/08/16.
//  Copyright © 2016 Tamási. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

public class NetworkManager
{
    public static func fetchTeams(page: Int, completion: (success: Bool, error: NSError?, response: JSON?) -> Void) {
        var url = BASE_URL.URLByAppendingPathComponent("/teams")
        url = url.URLByAppendingQueryString("page=\(page)&per_page=\(TEAMS_PER_PAGE)")
        Alamofire.request(.GET, url, headers: ALAMOFIRE_HEADERS).responseJSON { response in
            if(response.result.isFailure) {
                completion(success: false, error: response.result.error, response: nil)
            } else {
                if let jsonObject: AnyObject = response.result.value {
                    let json = JSON(jsonObject)
                    completion(success: true, error: nil, response: json)
                }
            }
        }
    }
    
    public static func fetchPlayers(page: Int, team: String, completion: (success: Bool, error: NSError?, response: JSON?) -> Void) {
        var url = BASE_URL.URLByAppendingPathComponent("/players")
        url = url.URLByAppendingQueryString("page=\(page)&team_id=\(team)&per_page=\(PLAYERS_PER_PAGE)")
        Alamofire.request(.GET, url, headers: ALAMOFIRE_HEADERS).responseJSON { response in
            if(response.result.isFailure) {
                completion(success: false, error: response.result.error, response: nil)
            } else {
                if let jsonObject: AnyObject = response.result.value {
                    let json = JSON(jsonObject)
                    completion(success: true, error: nil, response: json)
                }
            }
        }
    }

}