//
//  NBATeamsViewController.swift
//  NBATest
//
//  Created by Tamási Móni on 21/08/16.
//  Copyright © 2016 Tamási. All rights reserved.
//

import Foundation
import UIKit

class NBATeamsViewController: UITableViewController
{
    var teams: [Team]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Teams"
        self.teams = DataManager.sharedManager.getTeams()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        var controllerStack = self.navigationController!.viewControllers
        if controllerStack[0].isKindOfClass(SplashViewController) {
            controllerStack.removeAtIndex(0)
        }
        self.navigationController!.setViewControllers(controllerStack, animated:false)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.teams!.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell: TeamCell = tableView.dequeueReusableCellWithIdentifier( "TeamCell", forIndexPath: indexPath) as! TeamCell
        
        let team: Team = self.teams![indexPath.row]
        
        cell.name.text = team.name
        cell.nickname.text = team.nickname
        cell.setBallColor(team.color2, linesColor: team.color1)
        
        if indexPath.row == self.teams!.count - 3 {
            DataManager.sharedManager.getTeamsFromServer() {
                (success: Bool, error: NSError?) in
                if success {
                    self.teams = DataManager.sharedManager.getTeams()
                    self.tableView.reloadData()
                }
                else {
                    let alertController = UIAlertController(title: "Error", message: "Unfortunatelly some error occured: \(error?.localizedDescription)", preferredStyle: UIAlertControllerStyle.Alert)
                    let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil)
                    alertController.addAction(okAction)
                    self.presentViewController(alertController, animated: true, completion: nil)
                }
            }
        }
        
        return cell
    }

    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return 60.0
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        let team = self.teams![indexPath.row]
        tableView.deselectRowAtIndexPath(indexPath, animated: false)
        
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let playersViewController = mainStoryboard.instantiateViewControllerWithIdentifier("NBAPlayersViewController") as! NBAPlayersViewController
        playersViewController.team = team
        self.navigationController?.pushViewController(playersViewController, animated: true)
    }
}