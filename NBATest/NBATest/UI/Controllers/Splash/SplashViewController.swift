//
//  SplashViewController.swift
//  NBATest
//
//  Created by Tamási Móni on 21/08/16.
//  Copyright © 2016 Tamási. All rights reserved.
//

import Foundation
import UIKit

class SplashViewController: UIViewController
{
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DataManager.sharedManager.getTeamsFromServer() {
            (success: Bool, error: NSError?) in
            if success {               
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let teamsViewController = mainStoryboard.instantiateViewControllerWithIdentifier("NBATeamsViewController") as! NBATeamsViewController
                self.navigationController?.pushViewController(teamsViewController, animated: true)
            }
            else {
                let alertController = UIAlertController(title: "Error", message: "Unfortunatelly some error occured: \(error?.localizedDescription)", preferredStyle: UIAlertControllerStyle.Alert)
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil)
                alertController.addAction(okAction)
                self.presentViewController(alertController, animated: true, completion: nil)
            }
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBarHidden = true
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBarHidden = false
    }
    
}