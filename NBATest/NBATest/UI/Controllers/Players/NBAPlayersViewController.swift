//
//  NBAPlayersViewController.swift
//  NBATest
//
//  Created by Tamási Móni on 21/08/16.
//  Copyright © 2016 Tamási. All rights reserved.
//

import Foundation
import UIKit

class NBAPlayersViewController: UICollectionViewController
{
    var team: Team?
    var players: [Player] = [Player]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = self.team?.name
        
        
        
        if team == nil {
            return
        }
        self.getPlayers(true)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
    }

    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return self.players.count
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell
    {
        let cell: PlayerCell = collectionView.dequeueReusableCellWithReuseIdentifier("PlayerCell", forIndexPath: indexPath) as! PlayerCell
        
        let player = self.players[indexPath.row]
        cell.name.text = player.name
        cell.position.text = player.position
        cell.height.text = "\(player.height) \(player.heightUnit)"
        cell.weight.text = "\(player.weight) \(player.weightUnit)"
        cell.experience.text = "\(player.experience) " + (player.experience != 1 ? "years" : "year")
        
        if player.salary == 0 {
            cell.salary.text = ""
        }
        else {
            let formatter = NSNumberFormatter()
            formatter.numberStyle = .DecimalStyle
            cell.salary.text = "\(formatter.stringFromNumber(player.salary)!) \(player.salaryUnit)"
        }
        
        cell.uniformNumber.text = player.uniformNumber
        cell.draftTeam.text = player.draftTeam
        
        if indexPath.row == self.players.count - 1 {
            self.getPlayers(false)
        }
        
        return cell
    }
    
    private func getPlayers(isFirstPage: Bool)
    {
        DataManager.sharedManager.getPlayersOfTeamFromServer(self.team!.slug, isFirstPage: true) {
            (success: Bool, error: NSError?) in
            if success {
                self.players = DataManager.sharedManager.getPlayers()
                self.collectionView!.reloadData()
            }
            else {
                let alertController = UIAlertController(title: "Error", message: "Unfortunatelly some error occured: \(error?.localizedDescription)", preferredStyle: UIAlertControllerStyle.Alert)
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil)
                alertController.addAction(okAction)
                self.presentViewController(alertController, animated: true, completion: nil)
            }
        }
    }
}

extension NBAPlayersViewController: UICollectionViewDelegateFlowLayout
{
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize
    {
        return CGSize(width: self.view.bounds.width, height: self.collectionView!.contentSize.height)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat
    {
        return 0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat
    {
        return 0
    }
}


