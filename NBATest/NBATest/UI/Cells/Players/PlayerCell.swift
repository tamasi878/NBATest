//
//  PlayerCell.swift
//  NBATest
//
//  Created by Tamási Móni on 21/08/16.
//  Copyright © 2016 Tamási. All rights reserved.
//

import Foundation
import UIKit

class PlayerCell: UICollectionViewCell {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var position: UILabel!
    @IBOutlet weak var experience: UILabel!
    @IBOutlet weak var weight: UILabel!
    @IBOutlet weak var height: UILabel!
    @IBOutlet weak var salary: UILabel!
    @IBOutlet weak var uniformNumber: UILabel!
    @IBOutlet weak var draftTeam: UILabel!
}
