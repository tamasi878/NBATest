//
//  TeamCell.swift
//  NBATest
//
//  Created by Tamási Móni on 21/08/16.
//  Copyright © 2016 Tamási. All rights reserved.
//

import Foundation
import UIKit

class TeamCell: UITableViewCell {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var nickname: UILabel!
    @IBOutlet weak var ball: UIImageView!
    @IBOutlet weak var lines: UIImageView!
    
    func setBallColor(ballColor: UIColor, linesColor: UIColor) {
        var image = UIImage(named: "ball")!.imageWithRenderingMode(.AlwaysTemplate)
        self.ball.image = image
        self.ball.tintColor = ballColor
        
        image = UIImage(named: "lines")!.imageWithRenderingMode(.AlwaysTemplate)
        self.lines.image = image
        self.lines.tintColor = linesColor
    }
}
