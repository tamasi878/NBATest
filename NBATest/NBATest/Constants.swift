//
//  Constants.swift
//  NBATest
//
//  Created by Tamási Móni on 21/08/16.
//  Copyright © 2016 Tamási. All rights reserved.
//

import Foundation

let ACCESS_TOKEN = "127f71887822ffab0a0bb68970c5ec04"
let BASE_URL = NSURL(string:"https://www.stattleship.com/basketball/nba")!

let ALAMOFIRE_HEADERS = [
    "Authorization": "Token token=\(ACCESS_TOKEN)",
    "Accept" : "application/vnd.stattleship.com; version=1",
    "Content-Type" : "application/json"
]


let TEAMS_PER_PAGE = 40
let PLAYERS_PER_PAGE = 40